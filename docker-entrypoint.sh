#!/bin/bash
mkdir /root/.ssh/
cp /certs/authorized_keys /root/.ssh/
cp /certs/id_rsa /root/.ssh/
cp /certs/id_rsa.pub /root/.ssh/
chmod 700 /root/.ssh/*
ssh-keyscan -t rsa gitlab.com >> root/.ssh/known_hosts
git config --global user.email $GIT_EMAIL
git config --global user.name $GIT_USER

set -e

if [ $# -eq 0 ]
  then
    jupyter lab --ip=0.0.0.0 --NotebookApp.password=$JUPYTER_PASS --allow-root --no-browser &> /dev/null &
    code-server*/code-server --allow-http --user-data-dir /data /code
  else
    exec "$@"
fi