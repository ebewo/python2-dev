FROM debian:buster-slim

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN apt-get update --fix-missing && \
    apt-get install -y git wget curl nano python-pip \
    python python2.7-dev python-numpy python-matplotlib \
    python-pandas python-biopython python-scipy \
    python-xlsxwriter python-seaborn python-statsmodels python-deap && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    pip install pysam==0.11.2.2 jupyterlab pylint

RUN echo "export PYTHONPATH="/code/NGStoolkit"" >> /root/.bashrc

RUN curl -s https://api.github.com/repos/cdr/code-server/releases/latest \
    | grep "browser_download_url.*linux-x64.tar.gz" \
    | cut -d : -f 2,3 | tr -d \" \
    | wget -qi - && tar -xzvf code-server* && chmod +x code-server*/code-server

COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ADD ./code /code

ENTRYPOINT ["docker-entrypoint.sh"]